/**
 * Install PostgreSQL
 * Required if the module adds programs to other modules
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Jitsi_Meet module
 */

--
-- Fix #102 error language "plpgsql" does not exist
--

CREATE OR REPLACE LANGUAGE plpgsql;

/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Jitsi_Meet/Configuration.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Configuration.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Jitsi_Meet/Rooms.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Rooms.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 2, 'Jitsi_Meet/Rooms.php', 'Y', null
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Rooms.php'
    AND profile_id=2);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Jitsi_Meet/Meet.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Meet.php'
    AND profile_id=1);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 2, 'Jitsi_Meet/Meet.php', 'Y', null
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Meet.php'
    AND profile_id=2);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 3, 'Jitsi_Meet/Meet.php', 'Y', null
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Meet.php'
    AND profile_id=3);

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 0, 'Jitsi_Meet/Meet.php', 'Y', null
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Jitsi_Meet/Meet.php'
    AND profile_id=0);


/**
 * config Table
 *
 * title: for ex.: 'JITSI_MEET_[your_config]'
 * value: string
 */
--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--
-- @link http://web.archive.org/web/20240111192951/https://jitsi.github.io/handbook/docs/community/community-instances/
-- Select random instance
-- @link https://stackoverflow.com/questions/14299043/postgresql-pl-pgsql-random-value-from-array-of-values#52944411

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_DOMAIN', (array[
    -- @link https://pads.ccc.de/jitsiliste
    'jitsi2.geeksec.de',
    -- @link https://gitlab.com/flavoursofopenscience/grav/-/raw/master/pages/02.community-run-open-source-tools-for-video-and-text-collaboration/20211226-jitsi-list-195-servers.txt
    'allo.bim.land',
    'besprechung.net',
    'calls.disroot.org',
    'conf.underworld.fr',
    'conference.facil.services',
    'freejitsi01.netcup.net',
    'jitsi.debamax.com',
    'jitsi.debian.social',
    'jitsi.dorf-post.de',
    'jitsi.fem.tu-ilmenau.de',
    'jitsi.ff3l.net',
    'jitsi.flyingcircus.io',
    'jitsi.freifunk-duesseldorf.de',
    'jitsi.hadoly.fr',
    'jitsi.hamburg.ccc.de',
    'jitsi.komuniki.fr',
    'jitsi.laas.fr',
    'jitsi.math.uzh.ch',
    'jitsi.milkywan.fr',
    'jitsi.mpi-bremen.de',
    'jitsi.nextmap.io',
    'jitsi.nluug.nl',
    'jitsi.php-friends.de',
    'jitsi.riot.im',
    'jitsi.tetaneutral.net',
    'jitsi.rptu.de',
    'meet.coredump.ch',
    'meet.cyon.tools',
    'meet.evolix.org',
    'meet.ffrn.de',
    'meet.freifunk-aachen.de',
    'meet.greenhost.net',
    'meet.greenmini.host',
    'meet.isf.es',
    'meet.kbu.freifunk.net',
    'meet.lug-stormarn.de',
    'meet.mayfirst.org',
    'meet.meerfarbig.net',
    'meet.roflcopter.fr',
    'meet.tedomum.net',
    'meet.vpsfree.cz',
    'meet.waag.org',
    'meet.weimarnetz.de',
    'meet.yapbreak.fr',
    'open.meet.garr.it',
    'smaug.lixper.it',
    'talk.fdn.fr',
    'vc.autistici.org',
    'open.meet.garr.it',
    'video.devloprog.org',
    'viko.extinctionrebellion.de',
    'virtual.chaosdorf.space',
    'webconf.viviers-fibre.net'
])[floor(random() * 62 + 1)]
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_DOMAIN');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_TOOLBAR', 'microphone,camera,hangup,desktop,fullscreen,profile,chat,recording,settings,raisehand,videoquality,tileview'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_TOOLBAR');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_SETTINGS', 'devices,language'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_SETTINGS');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_WIDTH', '100%'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_WIDTH');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_HEIGHT', '700'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_HEIGHT');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_BRAND_WATERMARK_LINK', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_BRAND_WATERMARK_LINK');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_DISABLE_VIDEO_QUALITY_LABEL', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_DISABLE_VIDEO_QUALITY_LABEL');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_JAAS_APP_ID', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_JAAS_APP_ID');

INSERT INTO config (school_id, title, config_value)
SELECT '0', 'JITSI_MEET_JAAS_JWT', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='JITSI_MEET_JAAS_JWT');

/**
 * Add module tables
 */

/**
 * Jitsi_Meet Rooms table
 */
--
-- Name: jitsi_meet_rooms; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS jitsi_meet_rooms (
    id serial PRIMARY KEY,
    -- school_id integer NOT NULL,
    syear numeric(4,0) NOT NULL,
    title text NOT NULL,
    subject text,
    password text,
    start_audio_only char(1),
    students text,
    users text,
    owner_id integer,
    created_at timestamp DEFAULT current_timestamp
);
