��    !      $  /   ,      �  c   �     M     Y     t  	   �     �     �     �  V   �       &   %     L  
   \     g     l     u     ~     �     �     �     �     �     �  M   �  :   (  !   c  +   �  �   �  �   z  9   I     �     �  �  �  m   �	     �	  !   �	     
     ,
      =
  $   ^
     �
  T   �
     �
  )   �
       
         +  	   4  	   >  #   H     l     u     z       
   �     �  _   �  6     '   D  )   l  �   �  �   ^  6   +     b     q        !                                                                                                                                 	            
          %s has invited you to join a meeting in room &quot;%s&quot;:
%s
<a href="%s">Enter the meeting</a>. Add Parents Add Selected Users to Room Add Students Add Users Brand Watermark Link Disable Video Quality Indicator Domain Every participant enters the room having enabled only their microphone. Camera is off. Height Hide/Show the video quality indicator. Invitation Sent Jitsi Meet Meet My Rooms New Room New meeting request in room: %s Remove Room Rooms Send Invitation Settings Start Audio Only The domain the Jitsi Meet server runs. Defaults to their free hosted service. The height in pixels or percentage of the embedded window. The link for the brand watermark. The module configuration has been modified. The settings available in comma separated format. For more information refer to <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L146">SETTINGS_SECTION</a>. The toolbar buttons to display in comma separated format. For more information refer to <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. The width in pixels or percentage of the embedded window. Toolbar Width Project-Id-Version: Jitsi Meet module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:11+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s vas je povabil, da se pridružite sestanku v sobi &quot;%s&quot;:
%s
<a href="%s">Vstopite v sestanek</a>. Dodaj Starše Dodajte izbrane uporabnike v sobo Dodaj Dijaka dodaj Uporabnike Povezava do vodnega žiga znamke Onemogoči indikator kakovosti videa Domena Vsak udeleženec vstopi v sobo z vključenim samo mikrofonom. Kamera je izklopljena. Višina Skrij/prikaži indikator kakovosti videa. Vabilo poslano Jitsi Meet Srečati Moje sobe Nova Soba Nova zahteva za sestanek v sobi: %s Odstrani Soba sobe Pošlji povabilo Nastavitve Zaženi Samo zvok Domena, ki jo izvaja strežnik Jitsi Meet. Privzeto je njihova brezplačna gostujoča storitev. Višina v slikovnih pikah ali odstotek vdelanega okna. Povezava za vodni žig blagovne znamke. Konfiguracija modula je bila spremenjena. Nastavitve so na voljo v obliki, ločeni z vejicami. Za več informacij glejte <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L146">SETTINGS_SECTION</a>. Gumbi orodne vrstice za prikaz v obliki, ločeni z vejicami. Za več informacij glejte <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a> Širina v slikovnih pikah ali odstotek vdelanega okna. Orodna vrstica Širina 