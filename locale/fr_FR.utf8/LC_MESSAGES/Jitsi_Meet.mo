��             +         �  c   �     -     9     T  	   a     k  5   �     �     �  V   �     4  &   ;     b     r     w     �     �     �     �     �     �     �     �  M   �  :   3  !   n  +   �  �   �  9   �     �     �  �  �  r   �     6	  3   J	     ~	     �	     �	  8   �	  +   
     0
  j   8
     �
  /   �
     �
  
   �
  
   �
               3     ;     A     H     _     k  a   �  >   �  $   )  -   N  �   |  >   o     �     �                                                             	                
                                                                   %s has invited you to join a meeting in room &quot;%s&quot;:
%s
<a href="%s">Enter the meeting</a>. Add Parents Add Selected Users to Room Add Students Add Users Brand Watermark Link Create an account at %s to get the AppID and the JWT. Disable Video Quality Indicator Domain Every participant enters the room having enabled only their microphone. Camera is off. Height Hide/Show the video quality indicator. Invitation Sent Meet My Rooms New Room New meeting request in room: %s Remove Room Rooms Send Invitation Settings Start Audio Only The domain the Jitsi Meet server runs. Defaults to their free hosted service. The height in pixels or percentage of the embedded window. The link for the brand watermark. The module configuration has been modified. The toolbar buttons to display in comma separated format. For more information refer to <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. The width in pixels or percentage of the embedded window. Toolbar Width Project-Id-Version: Jitsi Meet module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:10+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %s vous a invité à joindre la réunion dans la salle &quot;%s&quot; :
%s
<a href="%s">Entrer à la réunion</a>. Ajouter des parents Ajouter les utilisateurs sélectionnés à la salle Ajouter des élèves Ajouter des utilisateurs Lien de la marque en filigrane Créer un compte sur %s pour obtenir le AppID et le JWT. Désactiver l'indicateur de qualité vidéo Domaine Tous les participants entrent dans la salle avec seulement le microphone activé. La caméra est éteinte. Hauteur Montrer/cacher l'indicateur de qualité vidéo. Invitation envoyée Se réunir Mes salles Nouvelle salle Invitation réunion salle : %s Enlever Salle Salles Envoyer une invitation Paramètres Commencer en audio seulement Le domaine sur lequel se trouve le serveur Jitsi Meet. Par défaut, le service hébergé gratuit. La hauteur en pixels ou pourcentage de la fenêtre incrustée. Le lien pour la marque en filigrane. La configuration du module a été modifiée. Les boutons de la barre d'outils à afficher, au format séparé par une virgule. Pour plus d'informations, se référer à <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. La largeur en pixels ou pourcentage de la fenêtre incrustée. Barre d'outils Largeur 