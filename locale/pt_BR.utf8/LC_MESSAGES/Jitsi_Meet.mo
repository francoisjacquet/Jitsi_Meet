��            )         �  c   �          !     <  	   I     S     h     �  V   �     �  &   �          $     )     2     ;     [     b     g     m     }     �  M   �  :   �  !      +   B  �   n  9   =     w       �  �  p   z     �  (   �     #	     4	     H	  *   ^	     �	  _   �	     �	  3   �	     -
     =
     I
  	   V
  *   `
     �
     �
     �
     �
     �
     �
  b   �
  8   :     s  +   �  �   �  9   �     �     �           	                                                                      
                                                       %s has invited you to join a meeting in room &quot;%s&quot;:
%s
<a href="%s">Enter the meeting</a>. Add Parents Add Selected Users to Room Add Students Add Users Brand Watermark Link Disable Video Quality Indicator Domain Every participant enters the room having enabled only their microphone. Camera is off. Height Hide/Show the video quality indicator. Invitation Sent Meet My Rooms New Room New meeting request in room: %s Remove Room Rooms Send Invitation Settings Start Audio Only The domain the Jitsi Meet server runs. Defaults to their free hosted service. The height in pixels or percentage of the embedded window. The link for the brand watermark. The module configuration has been modified. The toolbar buttons to display in comma separated format. For more information refer to <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. The width in pixels or percentage of the embedded window. Toolbar Width Project-Id-Version: Jitsi Meet module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:10+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 %s convidou você para participar de uma reunião na sala &quot;%s&quot;:
%s
<a href="%s">Entre na reunião</a>. Adicionar pais Adicionar usuários selecionados à sala Adicionar alunos Adicionar usuários Link da marca d'água Desativar indicador de qualidade de vídeo Domínio Todos os participantes entram na sala apenas com o microfone ativado e câmera está desligada. Altura Mostrar/ocultar o indicador de qualidade do vídeo. Convite enviado Encontrarse Minhas salas Nova sala Nova solicitação de reunião na sala: %s Remover Sala Salas Enviar convite Configurações Iniciar apenas com áudio O domínio no qual o servidor Jitsi Meet é executado, por padrão, o serviço hospedado gratuito. A altura em pixels ou porcentagem da janela incorporada. O link da marca d'água. A configuração do módulo foi modificada. Os botões da barra de ferramentas devem ser exibidos em formato separado por vírgula. Para obter mais informações, consulte <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. A largura em pixels ou porcentagem da janela incorporada. Barra de ferramentas Largura 