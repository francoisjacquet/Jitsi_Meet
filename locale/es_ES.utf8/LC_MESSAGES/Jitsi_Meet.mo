��    !      $  /   ,      �  c   �     M     Y     t  	   �     �  5   �     �     �  V   �     T  &   [     �     �     �     �     �     �     �     �     �     �     �  M     :   S  !   �  +   �  �   �  �   �  9   t     �     �    �  h   �	     4
  ,   C
     p
     �
     �
  6   �
  -   �
       c        �  3   �     �     �     �     �  1   �     '     .     5     =     P     Y  ^   v  <   �       1   1  �   c  �   9  ;   (     d     z        !                                                                                                                              	   
                      %s has invited you to join a meeting in room &quot;%s&quot;:
%s
<a href="%s">Enter the meeting</a>. Add Parents Add Selected Users to Room Add Students Add Users Brand Watermark Link Create an account at %s to get the AppID and the JWT. Disable Video Quality Indicator Domain Every participant enters the room having enabled only their microphone. Camera is off. Height Hide/Show the video quality indicator. Invitation Sent Meet My Rooms New Room New meeting request in room: %s Remove Room Rooms Send Invitation Settings Start Audio Only The domain the Jitsi Meet server runs. Defaults to their free hosted service. The height in pixels or percentage of the embedded window. The link for the brand watermark. The module configuration has been modified. The settings available in comma separated format. For more information refer to <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L146">SETTINGS_SECTION</a>. The toolbar buttons to display in comma separated format. For more information refer to <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. The width in pixels or percentage of the embedded window. Toolbar Width Project-Id-Version: Jitsi Meet module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:09+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 %s le invita a unirse al encuentro en el salón &quot;%s&quot;:
%s
<a href="%s">Entrar al encuentro</a>. Agregar padres Agregar los Usuarios Seleccionados al Salón Agregar Estudiantes Agregar Usuarios Enlace de la Marca de Agua Crear una cuenta en %s para obtener el AppID y el JWT. Inhabilitar el Indicador de Calidad de Vídeo Dominio Todos los participantes entran al salón con solo el micrófono activado. La cámara está apagada. Altura Mostrar/Ocultar el indicador de calidad del vídeo. Invitación Enviada Encontrarse Mis Salones Nuevo Salón Nueva solicitación de encuentro en el salón: %s Quitar Salón Salones Enviar Invitación Opciones Comenzar con Audio Solamente El dominio en el cual el servidor Jitsi Meet corre. Por defecto es el servicio alojado gratis. La altura en píxeles o porcentaje de la ventana incrustada. El enlace de la marca de agua. La configuración del módulo ha sido modificada. Las opciones disponibles, en formato separado por coma. Para más informaciones, referirse a <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L58">SETTINGS_SECTIONS</a>. Los botones de la barra de herramientas que mostrar, en formato separado por coma. Para más informaciones, referirse a <a target="_blank" href="https://github.com/jitsi/jitsi-meet/blob/master/interface_config.js#L49">TOOLBAR_BUTTONS</a>. Lo ancho en píxeles o porcentaje de la ventana incrustada. Barra de herramientas Ancho 