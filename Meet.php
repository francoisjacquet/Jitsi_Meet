<?php
/**
 * Meet program
 *
 * @package Jitsi Meet module
 */

require_once 'modules/Jitsi_Meet/includes/common.fnc.php';
require_once 'modules/Jitsi_Meet/includes/Meet.fnc.php';

DrawHeader( ProgramTitle() );

$room_id = JitsiMeetCheckRoomID( issetVal( $_REQUEST['id'] ) );

if ( ! $room_id )
{
	// List user rooms.
	$user_rooms = JitsiMeetGetUserRooms();

	JitsiMeetUserRoomsMenuOutput( $user_rooms );
}
elseif ( $_REQUEST['modfunc'] === 'dynamic_js' )
{
	$params = JitsiMeetGetRoomParams( $room_id );

	ob_clean();

	if ( ! headers_sent() )
	{
		header( 'Content-Type: text/javascript' );
	}

	echo JitsiMeetDynamicJS( $params );

	die();
}
else
{
	// @since RosarioSIS 12.5 CSP remove unsafe-inline Javascript
	$dynamic_js_url = 'Modules.php?modname=' . $_REQUEST['modname'] . '&id=' . $room_id . '&modfunc=dynamic_js';
	?>
	<div id="meet"></div>
	<!-- 2024-12-17 Copied from @link https://8x8.vc/external_api.js -->
	<script src="modules/Jitsi_Meet/js/external_api.js"></script>
	<script src="<?php echo URLEscape( $dynamic_js_url ); ?>"></script>
	<?php
}
